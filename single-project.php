<?php
/**
 * Шаблон отдельной записи (single.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); ?>
<section id="single-project">
	<div class="container single-project">
			<?php if (have_posts()): ?>
				<?php while (have_posts()): the_post(); ?>

					<?php
						/** init */
						setPostViews(get_the_ID());
						$id = get_the_ID();
						$link_project = (get_post_meta(get_the_id(), 'link', true) ? get_post_meta(get_the_id(), 'link', true) : false);
                        $project_terms = get_the_terms(get_the_id(), 'project-cat');

						/** related projects */
						$slugs = '';
						foreach ($project_terms as $number => $term) {
							$slugs .= $term->slug.',';
						}
					?>

                    <?php get_template_part('part/breadcrumbs'); ?>

					<!-- Title -->
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 title">
							<h2 class="title"><?= get_the_title(); ?></h2>
						</div>
					</div>
					<!-- /Title -->

					<!-- Content -->
					<div class="row content">
                        
                        <?php
                            $project_images = json_decode(get_post_meta(get_the_id(), 'images', true));
                            $project_images[] = get_the_post_thumbnail_url(get_the_id(), 'full');
                        ?>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 left">
							<div class="carousel">
                                <?php foreach ($project_images as $number => $image_url): ?>
                                    <div class="img">
                                        <img src="<?= $image_url; ?>" alt="<?= get_the_title(); ?>" />
                                    </div>
                                <?php endforeach; ?>
                            </div>
							<div class="action">
								<div class="order">
                                    <button class="btn btn-order" data-toggle="modal" data-target="#order-modal">Order the project</button>
                                </div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 right">
							<div class="desc"><?= get_the_content(); ?></div>
							<?php if ($link_project): ?>
								<div class="link-project">View project: <a target="_blank" href="<?= $link_project; ?>" title="View"><?= $link_project; ?></a></div>
							<?php endif; ?>
						</div>

					</div>
					<!-- /Content -->

				<?php endwhile; ?>

				<!-- Navigation -->
				<div class="row navigation">

					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 previous">
						<?php previous_post_link('%link', '<span class="fa fa-chevron-left"></span> Previous project'); ?>
					</div>

					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 next">
						<?php next_post_link('%link', 'Next project <span class="fa fa-chevron-right"></span>'); ?>
					</div>

				</div>
				<!-- /Navigation -->

                <?php
                $related_projects = query_posts(array(
                    'project-cat' 		=> $slugs,
                    'post_type' 		=> 'project',
                    'numberposts' 		=> -1,
                    'post__not_in'		=> array($id),
                ));
                ?>
                <?php if ($related_projects): ?>
                    <!-- Related projects -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 related-projects">
                            <h3 class="title">Related projects</h3>
                            <div class="carousel">
                                <?php foreach ($related_projects as $number => $project): ?>
                                    <div class="item">
                                        <div class="action">
                                            <div class="link"><a href="<?= get_permalink($project->ID); ?>" title="More" class="btn btn-order">More</a></div>
                                        </div>
                                        <div class="img">
                                            <img src="<?= get_the_post_thumbnail_url($project->ID, 'full'); ?>" alt="<?= $project->post_title; ?>" />
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <!-- /Related projects -->
                <?php endif; ?>

                <?php
                $args = array(
                    'taxonomy' => 'project-cat',
                );
                $categories = get_terms( $args );
                ?>
                <?php if ($categories): ?>
                    <!-- Categories -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 categories">
                            <h3 class="title">Categories</h3>
                            <div class="carousel">
                                <?php foreach ($categories as $number => $category): ?>
                                    <div class="item">
                                        <div class="action">
                                            <div class="title"><h4 class="title white"><?= $category->name; ?></h4></div>
                                            <div class="desc"><?= crop_string($category->description, 250); ?></div>
                                            <div class="link"><div class="link"><a href="<?= get_term_link($category->term_id); ?>" title="More" class="btn btn-order">More</a></div></div>
                                        </div>
                                        <div class="img">
                                            <img src="<?= get_term_meta($category->term_id, 'project_img_value', 1); ?>" alt="<?= $category->name; ?>" />
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <!-- /Categories -->
                <?php endif; ?>

			<?php else: ?>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Post is not found.</div>
			<?php endif; ?>
		</div>
</section>
<?php get_footer(); ?>
