<?php
add_action('admin_menu', 'register_my_custom_submenu_page');

function register_my_custom_submenu_page() {
	add_menu_page( 'Настройки темы', 'Настройки темы', 'edit_published_posts', 'my_theme_options', 'options_print' );

}

function options_print()
{
	?> <div class="wrap">
		<h2><?php echo get_admin_page_title() ?></h2>

		<form action="options.php" method="POST">
			<?php
				settings_fields( 'main-setting' );     // скрытые защитные поля
				do_settings_sections( 'Настройки темы' ); // секции с настройками (опциями). У нас она всего одна 'section_id'
				submit_button();
			?>
		</form>
	</div>
	<?php
}

add_action('admin_init', 'main_setting');
function main_setting(){
	register_setting('main-setting', 'main_option'); //регистрация главных настроек
	add_settings_section('section_info', 'Главные настройки', '', 'Настройки темы'); //добавление секции для главных настроек
	add_settings_section('section_link', 'Настройки ссылок', '', 'Настройки темы'); 
	add_settings_field('phone_1','Телефон 1', 'fill_phone','Настройки темы', 'section_info',array('name' => 'phone_1'));
	add_settings_field('phone_2','Телефон 2', 'fill_phone','Настройки темы', 'section_info',array('name' => 'phone_2'));
	add_settings_field('email','Адрес эл. почты', 'fill_email','Настройки темы', 'section_info');
	add_settings_field('web','Web', 'fill_web','Настройки темы', 'section_info');
	foreach (array('linkedin','twitter','facebook') as $value) {
	 	add_settings_field($value, $value, 'fill_link', 'Настройки темы', 'section_link',array('type' => $value));
	 } 
}
function fill_link($arg){
	$name = $arg['type'];
	$val = get_option('main_option');
	$val = $val[$name];
	?><label>
		<input type="text" name="main_option[<?=$name?>]" value="<?php echo esc_attr( $val ) ?>" />
		<p class="description" id="tagline-description">Ссылка на <?=$name;?>.</p>
	</label>
	<?php
}
function fill_phone($arg){
	$name = $arg['name'];
	$val = get_option('main_option');
	$val = $val[$name];
	?>
	<label>
		<input type="text" name="main_option[<?=$name?>]" value="<?php echo esc_attr( $val ) ?>" />
		<p class="description" id="tagline-description">Введите номер телефона.</p>
	</label>
	<?php

}
function fill_email(){
	$val = get_option('main_option');
	$val = $val['email'];
	?>
	<label>
		<input type="text" name="main_option[email]" value="<?php echo esc_attr( $val ) ?>" />
		<p class="description" id="tagline-description">Введите имейл.</p>
	</label>
	<?php
}
function fill_web(){
	$val = get_option('main_option');
	$val = $val['web'];
	?>
	<label>
		<input type="text" name="main_option[web]" value="<?php echo esc_attr( $val ) ?>" />
		<p class="description" id="tagline-description">Введите Web.</p>
	</label>
	<?php
}