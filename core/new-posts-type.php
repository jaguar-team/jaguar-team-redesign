<?php
register_post_type('team',
    array(
      	'labels' => array(
			'name'               => 'Сотрудники', // основное название для типа записи
			'singular_name'      => 'Сотрудник', // название для одной записи этого типа
			'add_new'            => 'Добавить сотрудника', // для добавления новой записи
			'add_new_item'       => 'Добавление нового сотрудника', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Страница редактирования сотрудника компании', // для редактирования типа записи
			'search_items'       => 'Найти сотрудника компании', // для поиска по этим типам записи
			'not_found'          => 'Сотрудник компании не найден', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Сотрудников компании нет в корзине', // если не было найдено в корзине
			'menu_name'          => 'Сотрудники', // название меню
		),
      'public' => true,
      'publicly_queryable' => true,
      'has_archive' => false,
      'menu_position'		=> 4,
      'menu_icon'           => 'dashicons-groups',
      'supports'            => array('title','editor', 'thumbnail',), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
    )
);

register_post_type('project',
    array(
      	'labels' => array(
			'name'               => 'Проэкты', // основное название для типа записи
			'singular_name'      => 'Проэкт', // название для одной записи этого типа
			'add_new'            => 'Добавить проэкт', // для добавления новой записи
			'add_new_item'       => 'Добавление нового проэкта', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Страница редактирования проэкта', // для редактирования типа записи
			'search_items'       => 'Найти проэкт', // для поиска по этим типам записи
			'not_found'          => 'Проэкт не найден', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Проэктов нет в корзине', // если не было найдено в корзине
			'menu_name'          => 'Проэкты', // название меню
		),
      'public' => true,
      'publicly_queryable' => true,
      'has_archive' => false,
      'menu_position'		=> 5,
      'menu_icon'           => 'dashicons-images-alt',
      'supports'            => array('title','editor', 'thumbnail', ), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
    )
);

//Добавление таксономии категории к товарам
function create_taxonomy(){
    // заголовки
    $labels = array(
        'name'              => 'Категории проэктов',
        'singular_name'     => 'Категория проэкта',
        'search_items'      => 'Найти категории',
        'all_items'         => 'Все категории',
        'parent_item'       => 'Категория',
        'parent_item_colon' => 'Категория:',
        'edit_item'         => 'Редактировать данные категории проэкта',
        'update_item'       => 'Обновить данные категории проэкта',
        'add_new_item'      => 'Добавить новую категорию проэкта',
        'new_item_name'     => 'Новая категория проэкта',
        'menu_name'         => 'Категория',
    );
    // параметры
    $args = array(
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => $labels,
        'description'           => '', // описание таксономии
        'public'                => true,
        'publicly_queryable'    => null, // равен аргументу public
        'show_in_nav_menus'     => true, // равен аргументу public
        'show_ui'               => true, // равен аргументу public
        'show_tagcloud'         => true, // равен аргументу show_ui
        'hierarchical'          => false,
        'update_count_callback' => '',
        'rewrite'               => true,
        'hierarchical'          => true,
        'capabilities'          => array(),
        'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
        'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
        '_builtin'              => false,
        'show_in_quick_edit'    => null, // по умолчанию значение show_ui
    );
    register_taxonomy('project-cat', array('project'), $args );
}
add_action('init', 'create_taxonomy');

function create_taxonomy_tag(){
    // заголовки
    $labels = array(
        'name'              => 'Теги проэктов',
        'singular_name'     => 'Тег проэкта',
        'search_items'      => 'Найти теги',
        'all_items'         => 'Все теги',
        'parent_item'       => 'Тег',
        'parent_item_colon' => 'Тег:',
        'edit_item'         => 'Редактировать данные тега проэкта',
        'update_item'       => 'Обновить данные тега проэкта',
        'add_new_item'      => 'Добавить новый тег проэкта',
        'new_item_name'     => 'Новый тег проэкта',
        'menu_name'         => 'Тег',
    );
    // параметры
    $args = array(
        'label'                 => '', // определяется параметром $labels->name
        'labels'                => $labels,
        'description'           => '', // описание таксономии
        'public'                => true,
        'publicly_queryable'    => null, // равен аргументу public
        'show_in_nav_menus'     => true, // равен аргументу public
        'show_ui'               => true, // равен аргументу public
        'show_tagcloud'         => true, // равен аргументу show_ui
        'hierarchical'          => false,
        'update_count_callback' => '',
        'rewrite'               => true,
        'hierarchical'          => false,
        'capabilities'          => array(),
        'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
        'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
        '_builtin'              => false,
        'show_in_quick_edit'    => null, // по умолчанию значение show_ui
    );
    register_taxonomy('project-tag', array('project'), $args );
}
add_action('init', 'create_taxonomy_tag');

register_post_type('task',
    array(
      	'labels' => array(
			'name'               => 'Задачи', // основное название для типа записи
			'singular_name'      => 'Задача', // название для одной записи этого типа
			'add_new'            => 'Добавить задачу', // для добавления новой записи
			'add_new_item'       => 'Добавление новой задачи', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Страница редактирования задачи', // для редактирования типа записи
			'search_items'       => 'Найти задачу', // для поиска по этим типам записи
			'not_found'          => 'Задача не найдена', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Задач нет в корзине', // если не было найдено в корзине
			'menu_name'          => 'Задачи', // название меню
		),
      'public' => true,
      'publicly_queryable' => true,
      'has_archive' => false,
      'menu_position'		=> 6,
      'menu_icon'           => 'dashicons-welcome-learn-more',
      'supports'            => array('title','editor', 'thumbnail', 'comments', ), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
    )
);

register_post_type('tehnolegy',
    array(
      	'labels' => array(
			'name'               => 'Технологии', // основное название для типа записи
			'singular_name'      => 'Технология', // название для одной записи этого типа
			'add_new'            => 'Добавить технологию', // для добавления новой записи
			'add_new_item'       => 'Добавление новой технологии', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Страница редактирования технологии', // для редактирования типа записи
			'search_items'       => 'Найти технологию', // для поиска по этим типам записи
			'not_found'          => 'Технология не найдена', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Технологии нет в корзине', // если не было найдено в корзине
			'menu_name'          => 'Технологии', // название меню
		),
      'public' => true,
      'publicly_queryable' => true,
      'has_archive' => false,
      'menu_position'		=> 7,
      'menu_icon'           => 'dashicons-lightbulb',
      'supports'            => array('title',), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
    )
);
