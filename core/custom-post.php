<?php
// подключаем функцию активации мета блока (my_extra_fields)

add_action('add_meta_boxes', 'my_extra_fields', 1);


function my_extra_fields() {
	add_meta_box( 'extra_fields', 'Информация о работнике', 'fields_team', 'team', 'normal', 'high'  );
	add_meta_box( 'extra_fields', 'Информация о проэкте', 'fields_project', 'project', 'normal', 'high'  );
	add_meta_box( 'extra_fields', 'Информация о задаче', 'fields_task', 'task', 'normal', 'high'  );
}

function fields_team($post) {
	?>

	<p>
		<label>Специализация</label>
		<input type="text" name="team[specialization]" value="<?= get_post_meta($post->ID, 'specialization', 1); ?>"/>
	</p>
	<p>
		<label>Facebook</label>
		<input type="text" name="team[facebook]" value="<?= get_post_meta($post->ID, 'facebook', 1); ?>"/>
	</p>
	<p>
		<label>Google</label>
		<input type="text" name="team[google]" value="<?= get_post_meta($post->ID, 'google', 1); ?>"/>
	</p>
	<p>
		<label>Skype</label>
		<input type="text" name="team[skype]" value="<?= get_post_meta($post->ID, 'skype', 1); ?>"/>
	</p>
	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />

	<input name="save" type="submit" class="button button-primary button-large" value="Сохранить">

	<?php
}

function fields_task($post) {
	?>
	<p>
		<p>Решение задачи</p>
		<textarea name="task[answer]" rows="20" cols="110"><?=get_post_meta($post->ID, 'answer', 1);?></textarea>
	</p>
	<p>
		<label>Выполняющий задание</label>
		<select name="task[employe]">
			<?php $task_employee = get_posts(array('numberposts' => -1, 'post_type' => 'team'));
			 foreach ($task_employee as $value):?>
			<option value="<?=$value->post_title?>" <?php selected( get_post_meta($post->ID, 'employe', 1), $value->post_title ); ?>><?=$value->post_title?></option>
		<?php endforeach;?>
		</select>
	</p>
	<p>
		<label>Состояние</label>
		<select name="task[status]">
			<option value="Не выбрано" <?php selected(get_post_meta($post->ID, 'status', 1), 'Не выбрано'); ?>>Не выбрано</option>
			<option value="В процессе" <?php selected(get_post_meta($post->ID, 'status', 1), 'В процессе'); ?>>В процессе</option>
			<option value="Выполнено" <?php selected(get_post_meta($post->ID, 'status', 1), 'Выполнено'); ?>>Выполнено</option>
			<option value="На проверку" <?php selected(get_post_meta($post->ID, 'status', 1), 'На проверку'); ?>>На проверку</option>
		</select>
	</p>
	<p>

		<label>Файл</label>
		<input type="file" name="file_task" id="file_task"  multiple="false" />
	</P>
	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />

	<input name="save" type="submit" class="button button-primary button-large" value="Сохранить">
<?php
}

function fields_project($post) {
	$images = json_decode(get_post_meta( $post->ID, 'images', 1 ));

    ?>    

	<p>
		<label>Ссылка на проэкт</label>
		<input type="text" name="project[link]" value="<?= get_post_meta($post->ID, 'link', 1); ?>"/>
	</p>
	<p>
		<p>Изображения проэка:</p>
		<?php if (isset($images)): ?>
	        <?php foreach ($images as $key => $images): ?>
	            <img style="height: 100px; width:100px;" src="<?= $images; ?>" alt="" />
	        <?php endforeach; ?>	    
		<?php else: ?>
			<p>Изображение не выбранно. Выберите в правой колонке</p>
		<?php endif; ?>
	</p>  
	<p>
		<label>Сотрудники</label>
		<select size="5" multiple name="project[employe][]" width="30px">
			<?php $task_employee = get_posts(array('numberposts' => -1, 'post_type' => 'team'));
			 foreach ($task_employee as $value):?>
			<option value="<?=$value->post_title?>" <?php selected( get_post_meta($post->ID, 'employe', 1), $value->post_title ); ?>><?=$value->post_title?></option>
		<?php endforeach;?>
		</select>
	</p>
	<p>
		<label>Выбранные сотрудники для выполнения проэкта</label>
		<p>
			<?php $employe = get_post_meta($post->ID, 'employe', 1);
				if (!empty($employe)) {
					foreach ($employe as $value) {
						echo $value.' ';
					}
				} else {
					echo 'Сотрудники не выбранны';
				}
			?>
		</p>
	</p>
	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />


<?php
}
function project_images_extra_fields() {
    add_meta_box(
        'side_images_product',
        'Изображения проэкта',
        'project_side_images_fields',
        'project',
        'side',
        'low'
    );
}

add_action( 'add_meta_boxes', 'project_images_extra_fields', 1 );

//Функция добавления галереи к фасадам
function project_side_images_fields($post) {
    wp_enqueue_media(); ?>
    <p>
        <button class="button button-primary add_images">Добавить изображения</button>
        <input class="input_images" type="hidden" name="project[images]" value="">
    </p>
    <script>
        jQuery(document).ready(function(){

            jQuery('.add_images').click(function(e) {
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: true
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first();
                        // We convert uploaded_image to a JSON object to make accessing it easier
                        // Output to the console uploaded_image
                        var array_object = image.state().get('selection').toJSON();

                        var image_url_array = [];
                        if(array_object.length > 7) {

                            var image_urls = '';
                        }
                        else {
                            for(var i = 0; i < array_object.length; i++) {
                                image_url_array.push(array_object[i].url);
                            }

                            var image_urls = JSON.stringify(image_url_array);
                        }
                        // Let's assign the url value to the input field
                        jQuery('.input_images').val(image_urls);
                    });
            });
        });
    </script>

    <?php
}


add_action('save_post', 'update_fields_team', 0);
/* Сохраняем данные, при сохранении поста */
function update_fields_team($post_id ) {

	if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false; // проверка
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false; // если это автосохранение
	if ( !current_user_can('edit_post', $post_id) ) return false; // если юзер не имеет право редактировать запись

	if( !isset($_POST['team']) ) return false;

	// Все ОК! Теперь, нужно сохранить/удалить данные
	// $_POST['credit_company'] = array_map('trim', $_POST['extra']);
	foreach( $_POST['team'] as $key=>$value ){
		if( empty($value) ){
			delete_post_meta($post_id, $key); // удаляем поле если значение пустое
			continue;
		}

		update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
	}
	return $post_id;
}



add_action('save_post', 'update_fields_project', 0);
function update_fields_project($post_id ) {
	if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false; // проверка
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false; // если это автосохранение
	if ( !current_user_can('edit_post', $post_id) ) return false; // если юзер не имеет право редактировать запись

	if( !isset($_POST['project']) ) return false;

	// Все ОК! Теперь, нужно сохранить/удалить данные
	// $_POST['credit_company'] = array_map('trim', $_POST['extra']);
	foreach( $_POST['project'] as $key=>$value ) {
		if( empty($value) && $key != 'images' ) {
			delete_post_meta($post_id, $key); // удаляем поле если значение пустое
			continue;
		}
		if ($key != 'images') {
			update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
		}
		if ($key == 'images' && !empty($value)) {
			update_post_meta($post_id, $key, $value);
		}
	}
	return $post_id;
}

add_action('save_post', 'update_fields_task', 0);
function update_fields_task($post_id ) {
	if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false; // проверка
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false; // если это автосохранение
	if ( !current_user_can('edit_post', $post_id) ) return false; // если юзер не имеет право редактировать запись

	if( !isset($_POST['task']) ) return false;

	// Все ОК! Теперь, нужно сохранить/удалить данные
	// $_POST['credit_company'] = array_map('trim', $_POST['extra']);
	foreach( $_POST['task'] as $key=>$value ){
		if( empty($value) ){
			delete_post_meta($post_id, $key); // удаляем поле если значение пустое
			continue;
		}

		update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
	}
	return $post_id;
}

add_action("project-cat_edit_form_fields", 'add_new_custom_fields_project_cat');
function add_new_custom_fields_project_cat($term){
    wp_enqueue_media();
    $harak = get_term_meta( $term->term_id, 'project_img_value', 1 );
    ?>

   
    <p>
    <table id="table_harak" style="text-align: center; vertical-align: middle;">
        <h2>Изображение</h2>
        <tr>
            <th></th>
            <th>Фото</th>
            <th></th>
        </tr>
        <tr>
            <td><button class="add_img">Выбрать файл</button></td>
            <td><img src="<?= $harak; ?>"></td>
            <td><input type="hidden" value="<?= $harak?>" name="project_img_value"></td>
        </tr>
    </table>
    </p>
    <script>
        jQuery(document).ready(function($){

            $('.add_img').live('click', function(e) {
                var selector = jQuery(this);
                e.preventDefault();
                var image = wp.media({
                    title: 'Загрузить изображения',
                    multiple: false
                }).open()
                    .on('select', function(e){
                        // This will return the selected image from the Media Uploader, the result is an object
                        var uploaded_image = image.state().get('selection').first().toJSON().url;
                        // We convert uploaded_image to a JSON object to make accessing it easier
                        // Let's assign the url value to the input field
                        
                        selector.parent().parent().find('input[type="hidden"]').val(uploaded_image);
                    });
            }); 
        });
    </script>


<?php 
}

//Обновление произвольных полей категории фасадов
function save_custom_taxonomy_meta($term_id){

    
    $extra = array_map('trim', $_POST['project-cat']);
    
              if(isset($_POST['project_img_value'])) {

        $value = $_POST['project_img_value'];
        update_term_meta($term_id, 'project_img_value', $value);
    }

    foreach( $extra as $key => $value ){
        if( empty($value) ){
            delete_term_meta( $term_id, $key ); // удаляем поле если значение пустое
            continue;
        }

        update_term_meta( $term_id, $key, $value ); // add_term_meta() работает автоматически
    }   

    return $term_id;
}
add_action("edited_project-cat", 'save_custom_taxonomy_meta');