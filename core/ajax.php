<?php 

function is_email_valid($email) {
  return preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $email);
}

function is_phone($phone) {
  return preg_match("#^[-+0-9()\s]+$#",$phone);
}

function is_valid_name($name) {
        return preg_match('/^[A-Za-zА-Яа-яЁё]+[\s\,\.\-]*/', trim($name));
}

/** send form to email  **/
function my_action_send_mail() {

	$headers = array();
	$results = array();
	$results['status'] = true;

	$email 		= $_POST['email'];
	$to 		= 'zubkov96.andrey@gmail.com';
	$subject 	= 'Order project';
	$headers[] = 'From: Jaguar-team <me@example.net>';
	$headers[] 	= 'content-type: text/html';
	$name 		= $_POST['name'];
	$phone 		= $_POST['phone']; 
	$message = 'email : '.$email.' name : '.$name.' phone : '.$phone;

	if(!is_email_valid($email)) {
		$results['status'] = false;
		$results['message']['errors'][] = 'Incorrect email';
	}
	if(!is_phone($phone)) {
		$results['status'] = false;
		$results['message']['errors'][] = 'Incorrect phone';
	}
	if(!is_valid_name($name)) {
		$results['status'] = false;
		$results['message']['errors'][] = 'Incorrect name';
	}
	if($results['status']) {
		if(wp_mail( $to, $subject, $message, $headers)) {
			$results['message']['success'] = 'Success send order project';
		} else {
			$results['status'] = false;
			$results['message']['errors'][] = 'Failed send mail';
		}
	}
	wp_send_json($results);
	wp_die();
}

add_action('wp_ajax_send_mail', 'my_action_send_mail');
add_action('wp_ajax_nopriv_send_mail', 'my_action_send_mail');



