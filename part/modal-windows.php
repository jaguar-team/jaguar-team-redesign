<!-- Modal Order -->
<div class="modal fade" id="order-modal" tabindex="-1" role="dialog" aria-labelledby="orderModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="fa fa-times"></span></button>
                <h3 class="title">Order form</h3>
            </div>
            <div class="modal-body">
                <div class="alert status"></div>
                <form id="order-modal-form" action="" method="POST">

                    <div class="form-group">
                        <label for="modal-order-input-name">Name</label>
                        <input name="order-name" type="text" class="form-control" id="modal-order-input-name" placeholder="Enter your name">
                    </div>

                    <div class="form-group required">
                        <label class="control-label" for="modal-order-input-email">E-mail</label>
                        <input name="order-email" type="email" class="form-control" id="modal-order-input-email" placeholder="Enter your email" required>
                    </div>

                    <div class="form-group">
                        <label for="modal-order-input-phone">Phone</label>
                        <input name="order-phone" type="text" class="form-control" id="modal-order-input-phone" placeholder="Enter your phone">
                    </div>

                    <div class="text-center"><button class="btn btn-order" type="submit">Order project</button></div>

                </form>
            </div>
        </div>
    </div>
</div>