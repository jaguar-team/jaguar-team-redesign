<?php
/**
 * Created by PhpStorm.
 * User: vlad.dneprov
 * Date: 02.02.2017
 * Time: 15:03
 */
?>

<!-- Breadcrumbs -->
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 breadcrumb hidden-xs">
        <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs('<span class="fa fa-chevron-right"></span>'); ?>
    </div>
</div>
<!-- /Breadcrumbs -->
