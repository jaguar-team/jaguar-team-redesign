<?php
/**
 * Главная страница (index.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); // подключаем header.php ?> 
<section>
	<div class="container-fluid">
		<div class="row">
			<section>
				<div class="banner-img-container">
					<div class="banner-blackout-narrow">
						<div class="container">
							<h1>WELCOME TO OUR <span class="yellow">WEB STUDIO<span></h1>
							<p class="offset">THINK <span class="yellow">TWICE</span>,</p>
							<p>CODE ONCE</p>
						</div>
					</div>
				</div>
			</section>
		<div>
	</div> <!--container-fluid-->	
</section>
<section>
	<div class="grey-background">
		<div class="container">
			<div class="row">
				<?php $args = array(
						'numberposts' => -1,
						'orderby'     => 'date',
						'order'       => 'DESC',
						'post_type'   => 'project',
						);

					$projects = get_posts( $args );
					?>

				<h3 class="title">OUR PROJECTS</h3>
				<div class="project-slider">
					<?php foreach ($projects as $value): $images = json_decode(get_post_meta($value->ID, 'images',1));?>
					<a href="<?=$value->guid;?>" class="item">
						<div class="block">
							<div class="shadow img">
								<?= get_the_post_thumbnail($value->ID, 'full');?>
							</div>
						</div>
						<div class="block hidden-xs">
							<div class="img-block">
								<div class="shadow img">
									<img class="" src="<?=$images[0]?>">
								</div>
							</div>
							<div class="img-block">
								<div class="shadow img">
									<img class="" src="<?=$images[1]?>">
								</div>
							</div>
						</div>
					</a>
					<?php endforeach; ?>					
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="black-background">
		<div class="container">

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h3 class="title white">OUR TEAM</h3>
					<p class="team-description">Our team are all experts in their field - we employ only the best developers and designers. If you'd like to come in and meet us and have a chat about a new project, then please get in touch. Click on a member of the team to get in touch...
					</p>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 our-team">
					<?php
						$arg = array(
							'numberposts'	=> 0,
							'order_by'		=> 'date',
							'order'			=> 'ASC',
							'post_type'		=> 'team'
						);

						$team = get_posts($arg);

					foreach ($team as $number => $person) : ?>
						<div class="block">
							<div class="content">
								<div class="img">
									<?= get_the_post_thumbnail($person->ID, 'full'); ?>
									<div class="img-social">
										<ul>
											<li>
												<a href="<?= get_post_meta($person->ID, 'facebook', 1); ?>" title="Facebook">
													<img src="<?= get_template_directory_uri().'/img/facebook.png'; ?>" alt="Facebook" />
												</a>
											</li>
											<li>
												<a href="<?= get_post_meta($person->ID, 'google', 1); ?>" title="Google">
													<img src="<?= get_template_directory_uri().'/img/google.png'; ?>" alt="Google" />
												</a>
											</li>
											<li>
												<a href="tel:<?=get_post_meta($person->ID, 'skype', 1);?>" title="Skype">
													<img src="<?= get_template_directory_uri().'/img/skype.png'; ?>" alt="Skype" />
												</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="name"><?= $person->post_title; ?></div>
								<div class="title"><?= get_post_meta($person->ID, 'specialization', 1); ?></div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="bg-our-process">
		<div class="container">

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<h3 class="title white">Our process</h3>
				</div>
			</div>
	
			<div class="row hidden-xs">
				<ul class="nav nav-tabs col-xs-12 col-sm-12 col-md-12 col-lg-12 our-process" role="tablist">
					<li role="presentation" class="our-process-container active">
						<a class="our-process-circle" href="#meet" aria-controls="home" role="tab" data-toggle="tab">
							Meet & Agree
						</a>
					</li>
					<li role="presentation" class="our-process-container">
						<a class="our-process-circle" href="#idea" aria-controls="profile" role="tab" data-toggle="tab">
							Idea & Concept
						</a>
					</li>
					<li role="presentation" class="our-process-container">
						<a class="our-process-circle" href="#design" aria-controls="messages" role="tab" data-toggle="tab">
							Design & Create
						</a>
					</li>
					<li role="presentation" class="our-process-container">
						<a class="our-process-circle" href="#build" aria-controls="settings" role="tab" data-toggle="tab">
							Build & Install
						</a>
					</li>
				</ul>

				<div class="tab-content">
					<div role="tabpanel" class="tab-pane our-process-text-down active" id="meet">
						<p>Jaguar-team is the team of open minded specialists, who wants to deliver your ideas in life in shortest period of time. We are offering effective and most sutiable solutions to your needs.  Our team is always on the way of new technologies to solve your issues . At Jaguar-team, you can find professional and skilled help with the best concept for your project.</p>
					</div>
					<div role="tabpanel" class="tab-pane our-process-text-down" id="idea">
						<p>New clients, new ideas, new experience, new project. We provide core development needs. We also carry of individuals for the purpose of providing services in areas such as Backend, Frontend, API & Integration, and Design. With our friendly team you will be not only  in good mood, but  and with a perfect project.</p>
					</div>
					<div role="tabpanel" class="tab-pane our-process-text-down" id="design">
						<p>We can catch your ideas from almost one word and put them into mockups. We work carefully on the project with dedicated attention to our clients and give them an effective solution to all their needs. We are ready to share with you our vision and our experince to create excellent design. Be sure, that with our design you will stand out from your competitors.</p>
					</div>
					<div role="tabpanel" class="tab-pane our-process-text-down" id="build">
						<p>Be carefull, we are really interested to build a project in time and with exeptional quality. Our team is trying to be on wave of new tehnologies and use them in your products. Be sure that with us you will be always in a good mood, because we install everything immidiately to save your time, money and nerves.</p>
					</div>
				</div>
			</div>

			<div class="panel-group visible-xs" id="accordion" role="tablist" aria-multiselectable="true">
			  <div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingOne">
			      <h4 class="panel-title">
			        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			          Meet & Agree
			        </a>
			      </h4>
			    </div>
			    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
			      <div class="panel-body our-process-text-down">
			        <p>Jaguar-team is the team of open minded specialists, who wants to deliver your ideas in life in shortest period of time. We are offering effective and most sutiable solutions to your needs.  Our team is always on the way of new technologies to solve your issues . At Jaguar-team, you can find professional and skilled help with the best concept for your project.</p>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingTwo">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			          Idea & Concept
			        </a>
			      </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="panel-body our-process-text-down">
			        <p>New clients, new ideas, new experience, new project. We provide core development needs. We also carry of individuals for the purpose of providing services in areas such as Backend, Frontend, API & Integration, and Design. With our friendly team you will be not only  in good mood, but  and with a perfect project.</p>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingThree">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			          Design & Create
			        </a>
			      </h4>
			    </div>
			    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			      <div class="panel-body our-process-text-down">
			        <p>We can catch your ideas from almost one word and put them into mockups. We work carefully on the project with dedicated attention to our clients and give them an effective solution to all their needs. We are ready to share with you our vision and our experince to create excellent design. Be sure, that with our design you will stand out from your competitors.</p>
			      </div>
			    </div>
			  </div>
			  <div class="panel panel-default">
			    <div class="panel-heading" role="tab" id="headingTwo">
			      <h4 class="panel-title">
			        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
			          Build & Install
			        </a>
			      </h4>
			    </div>
			    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="panel-body our-process-text-down">
			        <p>Be carefull, we are really interested to build a project in time and with exeptional quality. Our team is trying to be on wave of new tehnologies and use them in your products. Be sure that with us you will be always in a good mood, because we install everything immidiately to save your time, money and nerves.</p>
			      </div>
			    </div>
			  </div>
			</div>

		</div>
	</div>
</section>

<section>
	<div class="bg-our-technologies">
		<div class="container">

			<div class="row our-technologies">
				<div class="container-tech">
					<div class="angular">Angular2</div>
				</div>
				<div class="container-tech">
					<div class="prestashop">Prestashop</div>
				</div>
				<div class="container-tech">
					<div class="yii">Yii2</div>
				</div>
				<div class="container-tech">
					<div class="wordpress">Wordpress</div>
				</div>
			</div>

		</div>
	</div>
</section>

<?php get_footer();  ?>