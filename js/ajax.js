jQuery(function ($) {
    $(document).ready(function () {

        /** order **/
        $('#order-modal-form').submit(function (e) {

            e.preventDefault();
            var data = {
                'action': 'send_mail',
                'name': $(this).find("input[name='order-name']").val(),
                'email': $(this).find("input[name='order-email']").val(),
                'phone': $(this).find("input[name='order-phone']").val()
            };

            $.ajax({
                url: update_task_meta.url,
                method: 'POST',
                data: data,
                success: function (response) {
                    console.log(response);
                    if (!response.status) {
                        var error_messages = '';
                        for (var i = 0; i < response.message.errors.length; i++) {
                            error_messages += '<p>' + response.message.errors[i] + '</p>';
                        }
                        $("#order-modal .modal-body .alert").addClass('alert-danger').css('display', 'block').html(error_messages);
                    }
                    else {
                        var success_message = response.message.success;
                        $("#order-modal .modal-body .alert").addClass('alert-success').css('display', 'block').text(success_message);

                        setTimeout(function () {

                            /** clear input val **/
                            var selector = $('#order-modal-form');
                            selector.find('input').val('');

                            /** close modal **/
                            $('#order-modal').modal('hide');
                            $("#order-modal .modal-body .alert").css('display', 'none');
                        }, 3000);
                    }
                }
            });
        });

    });
});