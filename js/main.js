jQuery(function ($) {
   $(document).ready(function () {
       $(".slicknav_collapsed").click(function (e) {
           e.preventDefault();
           if($(".mobile_menu").hasClass("hidden")){
               $(".mobile_menu").removeClass("hidden")
           }else{
               $(".mobile_menu").addClass("hidden")               
           }

       });

       /** slider **/

      $('.project-slider').slick({
        arrows: false,
        dots: false,
        autoplay: false,
        slidesToShow: 1
      });

       $('.our-technologies').slick({
           //dots: true,
           infinite: false,
           arrows: false,
           speed: 500,
           autoplay: true,
           autoplaySpeed: 3000,
           slidesToShow: 4,
           responsive: [
               {
                   breakpoint: 1024,
                   settings: {
                       slidesToShow: 3,
                       slidesToScroll: 3,
                       infinite: true,
                       dots: true
                   }
               },
               {
                   breakpoint: 600,
                   settings: {
                       slidesToShow: 2,
                       slidesToScroll: 2
                   }
               },
               {
                   breakpoint: 480,
                   settings: {
                       slidesToShow: 1,
                       slidesToScroll: 1
                   }
               }
           ]
       });

       $('.related-projects > .carousel, .categories > .carousel').slick({
           //dots: true,
           infinite: false,
           arrows: true,
           speed: 500,
           autoplay: false,
           autoplaySpeed: 3000,
           slidesToShow: 4,
           nextArrow: '<div class="arrow-next"><span class="fa fa-chevron-right"></span></div>',
           prevArrow: '<div class="arrow-prev"><span class="fa fa-chevron-left"></span></div>',
           responsive: [
               {
                   breakpoint: 991,
                   settings: {
                       slidesToShow: 3,
                       slidesToScroll: 3,
                       infinite: true,
                       dots: true
                   }
               },
               {
                   breakpoint: 769,
                   settings: {
                       slidesToShow: 2,
                       slidesToScroll: 2
                   }
               },
               {
                   breakpoint: 480,
                   settings: {
                       slidesToShow: 1,
                       slidesToScroll: 1,
                       arrows: false
                   }
               }
           ]
       });

       $('.single-project .content .left .carousel').slick({
           //dots: true,
           infinite: false,
           arrows: true,
           speed: 500,
           autoplay: false,
           autoplaySpeed: 3000,
           slidesToShow: 1,
           nextArrow: '<div class="arrow-next"><span class="fa fa-chevron-right"></span></div>',
           prevArrow: '<div class="arrow-prev"><span class="fa fa-chevron-left"></span></div>',
           responsive: [
               {
                   breakpoint: 1024,
                   settings: {
                       slidesToShow: 1,
                       slidesToScroll: 1,
                       arrows: false
                   }
               },
               {
                   breakpoint: 600,
                   settings: {
                       slidesToShow: 1,
                       slidesToScroll: 1,
                       arrows: false
                   }
               },
               {
                   breakpoint: 480,
                   settings: {
                       slidesToShow: 1,
                       slidesToScroll: 1,
                       arrows: false
                   }
               }
           ]
       });

   }); 
});