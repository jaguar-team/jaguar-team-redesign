<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
?>
	<footer>
		<?php $info = get_option('main_option'); ?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3 class="title">contact us</h3>			
				</div>
			</div>
			<div class="row">
				<form action="" class="contactUs">
					<div class="col-xs-6 col-sm-5 col-md-3">
						<input type="text" class="form-control" id="exampleInputName2" placeholder="Your name">
						<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Your e-mail">
						<input type="submit" class="btn btn-default" value="Send message">
					</div>
					<div class="col-xs-6 col-sm-7 col-md-6">
						<textarea class="form-control" placeholder="Your message..."></textarea>
					</div>
				</form>
				<div class="col-xs-12 col-sm-12 col-md-3">
					<div class="container-contact-info">
						<h4 class="h4-contact-info">Contact info</h4>
						<hr class="hr-contact-info">
						<ul class="ul-contact-info-head">
							<li><span>Phone:</span></li>
							<li><span>E-mail:</span></li>
							<li><span>Web:</span></li>
							<li><span>Social:</span></li>
						</ul>
						<ul class="ul-contact-info">
							<li><?=$info['phone_1'];?></li>
							<li><?=$info['email'];?></li>
							<li><?=$info['web'];?></li>
							<li><?=$info['twitter'];?></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 container-copyrating">
					<img class="footer-logo" src="<?=get_template_directory_uri();?>/img/head_logo.png">
					<span class="copyrating">Jaguar-team</span>
				</div>
			</div>
		</div>
	</footer>
<?php get_template_part('part/modal-windows'); ?>
<?php wp_footer(); ?>
</body>
</html>