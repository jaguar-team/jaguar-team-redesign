<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<?php wp_head(); ?>
</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<a href="<?=get_home_url();?>">
						<?php $info = get_option('main_option'); ?>
						<img class="head-logo" src="<?=get_template_directory_uri();?>/img/head_logo.png">
						<span id="blog-name"><?=get_bloginfo()?></span>
					</a>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5 header-contact">
							<ul>
								<li><a href="tel:<?=$info['phone_1'];?>" title="Call"><?=$info['phone_1'];?></a></li>
								<li><a href="tel:<?=$info['phone_2'];?>" title="Call"><?=$info['phone_2'];?></a></li>
								<li class="callback">
									<span class="fa fa-headphones"></span><a href="#" data-toggle="modal" data-target="#order-modal">Callback</a>
								</li>
							</ul>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-7 col-lg-7 hidden-xs">
							<?php wp_nav_menu( array(
								'location'			=> 'top',
								'container_class' 	=> 'menu-main-menu-container',
							) ); ?>
						</div>
					</div>
				</div>	
				
				<div class="dropdown mobile-nav col-xs-12 visible-xs">
				  <i class="slicknav_menu fa fa-bars" type="button" data-toggle="dropdown"></i>
				  <?php  wp_nav_menu( array('menu' => '2', 'menu_class' => 'mobile_menu dropdown-menu', 'container' => 'ul' ) ); ?>
				</div>		
			</div>	<!-- headRow -->
			
		</div>  <!-- container -->
	</header>
