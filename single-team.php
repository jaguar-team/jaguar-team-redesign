<?php
/**
 * Шаблон отдельной записи (single.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
get_header(); // подключаем header.php ?>
<section>
    <div class="container">
        <div class="row">
            <div class="<?php content_class_by_sidebar(); // функция подставит класс в зависимости от того есть ли сайдбар, лежит в functions.php ?>">
                <?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> <?php // контэйнер с классами и id ?>
                        <h1><?php the_title(); // заголовок поста ?></h1>
                        <div class="meta">
                          <?= get_the_post_thumbnail(get_the_id(),'full'); ?>
                        </div>
                        <p>Specialization : <?= get_post_meta(get_the_id(), 'specialization', 1); ?></p>
                        <p><a href="<?= get_post_meta(get_the_id(), 'facebook', 1); ?>" title="Facebook"> Faceboock</a></p>
                        <p><a href="<?= get_post_meta(get_the_id(), 'google', 1); ?>" title="Google"> Google</a></p>
                        <p><a href="tel:<?= get_post_meta(get_the_id(), 'skype', 1); ?>">Skype</a></p>
                    </article>
                <?php endwhile; // конец цикла ?>
                
            </div>
           
        </div>
    </div>
</section>
<?php get_footer(); // подключаем footer.php ?>
